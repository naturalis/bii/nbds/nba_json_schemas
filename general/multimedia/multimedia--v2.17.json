{
  "$id": "http://api.biodiversitydata.nl/v2/multimedia/",
  "version": "2.17",
  "type": "object",
  "properties": {
    "sourceSystem": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string",
          "enum": [
            "BRAHMS",
            "CRS",
            "NSR",
            "OBS",
            "XC"
          ]
        },
        "name": {
          "type": "string",
          "enum": [
            "Naturalis - Botany catalogues",
            "Naturalis - Zoology and Geology catalogues",
            "Naturalis - Dutch Species Register",
            "Observation.org - Nature observations",
            "Xeno-canto.org - Bird sounds"
          ]
        }
      },
      "required": [
        "code",
        "name"
      ],
      "additionalProperties": false
    },
    "id": {
      "type": "string"
    },
    "sourceSystemId": {
      "type": "string"
    },
    "sourceInstitutionID": {
      "type": "string",
      "enum": [
        "Naturalis Biodiversity Center",
        "Observation International",
        "Stichting Xeno-canto voor Natuurgeluiden"
      ]
    },
    "sourceID": {
      "type": "string",
      "enum": [
        "Brahms",
        "CRS",
        "LNG NSR",
        "Xeno-canto",
        "Observation.org"
      ]
    },
    "owner": {
      "type": "string"
    },
    "licenseType": {
      "type": "string"
    },
    "license": {
      "type": "string",
      "enum": [
        "CC0",
        "CC BY",
        "CC BY-SA",
        "CC BY-NC",
        "CC BY-NC-SA",
        "CC BY-ND",
        "CC BY-NC-ND",
        "CC0 1.0",
        "CC BY 1.0",
        "CC BY-SA 1.0",
        "CC BY-NC 1.0",
        "CC BY-NC-SA 1.0",
        "CC BY-ND 1.0",
        "CC BY-NC-ND 1.0",
        "CC BY 2.0",
        "CC BY-SA 2.0",
        "CC BY-NC 2.0",
        "CC BY-NC-SA 2.0",
        "CC BY-ND 2.0",
        "CC BY-NC-ND 2.0",
        "CC BY 2.5",
        "CC BY-SA 2.5",
        "CC BY-NC 2.5",
        "CC BY-NC-SA 2.5",
        "CC BY-ND 2.5",
        "CC BY-NC-ND 2.5",
        "CC BY 3.0",
        "CC BY-SA 3.0",
        "CC BY-NC 3.0",
        "CC BY-NC-SA 3.0",
        "CC BY-ND 3.0",
        "CC BY-NC-ND 3.0",
        "CC BY 4.0",
        "CC BY-SA 4.0",
        "CC BY-NC 4.0",
        "CC BY-NC-SA 4.0",
        "CC BY-ND 4.0",
        "CC BY-NC-ND 4.0",
        "All rights reserved"
      ]
    },
    "recordURI": {
      "type": "string",
      "format": "uri"
    },
    "unitID": {
      "type": "string"
    },
    "collectionType": {
      "type": "string"
    },
    "title": {
      "type": "string"
    },
    "caption": {
      "type": "string"
    },
    "description": {
      "type": "string"
    },
    "taxonCount": {
      "type": "integer"
    },
    "serviceAccessPoints": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "accessUri": {
            "type": "string",
            "format": "uri"
          },
          "format": {
            "type": "string",
            "enum": [
              "image/jpeg",
              "image/png",
              "audio/mp3",
              "video/mp4",
              "application/pdf"
            ]    
          },
          "variant": {
            "type": "string"
          }
        },
        "required": [
          "accessUri",
          "format",
          "variant"
        ],
        "additionalProperties": false
      }
    },
    "creator": {
      "type": "string"
    },
    "copyrightText": {
      "type": "string"
    },
    "associatedSpecimenReference": {
      "type": "string"
    },
    "associatedTaxonReference": {
      "type": "string"
    },
    "multiMediaPublic": {
      "type": "boolean"
    },
    "phasesOrStages": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "sexes": {
      "type": "array",
      "items": {
        "type": "string",
        "enum": [
        "female",
        "male",
        "hermaphrodite",
        "mixed",
        "unknowable",
        "undetermined"
        ]
      }
    },
    "gatheringEvents": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "behavior": {
            "type": "string"
          },
          "code": {
            "type": "string"
          },
          "establishmentMeans": {
            "type": "string"
          },
          "worldRegion": {
            "type": "string"
          },
          "continent": {
            "type": "string"
          },
          "country": {
            "type": "string"
          },
          "provinceState": {
            "type": "string"
          },
          "sublocality": {
            "type": "string"
          },
          "localityText": {
            "type": "string"
          },
          "projectTitle": {
            "type": "string"
          },
          "iso3166Code": {
            "type": "string"
          },
          "island": {
            "type": "string"
          },
          "locality": {
            "type": "string"
          },
          "city": {
            "type": "string"
          },
          "method": {
            "type": "string"
          },
          "altitudeUnifOfMeasurement": {
            "type": "string"
          },
          "depth": {
            "type": "string"
          },
          "depthUnitOfMeasurement": {
            "type": "string"
          },
          "dateTimeBegin": {
            "type": "string",
            "format": "date-time"
          },
          "dateTimeEnd": {
            "type": "string",
            "format": "date-time"
          },
          "gatheringPersons": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "fullName": {
                  "type": "string"
                },
                "agentText": {
                  "type": "string"
                },
                "organization": {
                  "type": "object",
                  "properties": {
                    "agentText": {
                      "type": "string"
                    },
                    "name": {
                      "type": "string"
                    }
                  },
                  "additionalProperties": false
                }
              },
              "additionalProperties": false
            }
          },
          "siteCoordinates": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "longitudeDecimal": {
                  "type": "number",
                  "minimum": -180,
                  "maximum": 180
                },
                "latitudeDecimal": {
                  "type": "number",
                  "minimum": -90,
                  "maximum": 90
                },
                "gridCellSystem": {
                  "type": "string"
                },
                "gridLatitudeDecimal": {
                  "type": "number"
                },
                "gridLongitudeDecimal": {
                  "type": "number"
                },
                "gridCellCode": {
                  "type": "string"
                },
                "gridQualifier": {
                  "type": "string"
                },
                "coordinateErrorDistanceInMeters": {
                  "type": "number"
                },
                "spatialDatum": {
                  "type": "string",
                  "enum": [
                    "WGS84",
                    "NAD83",
                    "NAD27"
                  ]
                }
              },
              "additionalProperties": false
            }
          },
          "chronoStratigraphy": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "youngRegionalSubstage": {
                  "type": "string"
                },
                "youngRegionalStage": {
                  "type": "string"
                },
                "youngRegionalSeries": {
                  "type": "string"
                },
                "youngDatingQualifier": {
                  "type": "string"
                },
                "youngInternSystem": {
                  "type": "string"
                },
                "youngInternSubstage": {
                  "type": "string"
                },
                "youngInternStage": {
                  "type": "string"
                },
                "youngInternSeries": {
                  "type": "string"
                },
                "youngInternErathem": {
                  "type": "string"
                },
                "youngInternEonothem": {
                  "type": "string"
                },
                "youngChronoName": {
                  "type": "string"
                },
                "youngCertainty": {
                  "type": "string"
                },
                "oldDatingQualifier": {
                  "type": "string"
                },
                "chronoPreferredFlag": {
                  "type": "boolean"
                },
                "oldRegionalSubstage": {
                  "type": "string"
                },
                "oldRegionalStage": {
                  "type": "string"
                },
                "oldRegionalSeries": {
                  "type": "string"
                },
                "oldInternSystem": {
                  "type": "string"
                },
                "oldInternSubstage": {
                  "type": "string"
                },
                "oldInternStage": {
                  "type": "string"
                },
                "oldInternSeries": {
                  "type": "string"
                },
                "oldInternErathem": {
                  "type": "string"
                },
                "oldInternEonothem": {
                  "type": "string"
                },
                "oldChronoName": {
                  "type": "string"
                },
                "chronoIdentifier": {
                  "type": "string"
                },
                "oldCertainty": {
                  "type": "string"
                }
              },
              "additionalProperties": false
            }
          },
          "lithoStratigraphy": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "qualifier": {
                  "type": "string"
                },
                "preferredFlag": {
                  "type": "boolean"
                },
                "member2": {
                  "type": "string"
                },
                "member": {
                  "type": "string"
                },
                "informalName2": {
                  "type": "string"
                },
                "informalName": {
                  "type": "string"
                },
                "importedName2": {
                  "type": "string"
                },
                "importedName1": {
                  "type": "string"
                },
                "lithoIdentifier": {
                  "type": "string"
                },
                "formation2": {
                  "type": "string"
                },
                "formationGroup2": {
                  "type": "string"
                },
                "formationGroup": {
                  "type": "string"
                },
                "formation": {
                  "type": "string"
                },
                "certainty2": {
                  "type": "string"
                },
                "certainty": {
                  "type": "string"
                },
                "bed2": {
                  "type": "string"
                },
                "bed": {
                  "type": "string"
                }
              },
              "additionalProperties": false
            }
          },
          "bioStratigraphy": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "youngBioDatingQualifier": {
                  "type": "string"
                },
                "youngBioName": {
                  "type": "string"
                },
                "youngFossilZone": {
                  "type": "string"
                },
                "youngFossilSubZone": {
                  "type": "string"
                },
                "youngBioCertainty": {
                  "type": "string"
                },
                "youngStratType": {
                  "type": "string"
                },
                "bioDatingQualifier": {
                  "type": "string"
                },
                "bioPreferredFlag": {
                  "type": "boolean"
                },
                "rangePosition": {
                  "type": "string"
                },
                "oldBioName": {
                  "type": "string"
                },
                "bioIdentifier": {
                  "type": "string"
                },
                "oldFossilzone": {
                  "type": "string"
                },
                "oldFossilSubzone": {
                  "type": "string"
                },
                "oldBioCertainty": {
                  "type": "string"
                },
                "oldBioStratType": {
                  "type": "string"
                }
              },
              "additionalProperties": false
            }
          },
          "biotopeText": {
            "type": "string"
          },
          "altitude": {
            "type": "string"
          },
          "dateText": {
            "type": "string"
          },
          "gatheringOrganizations": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "agentText": {
                  "type": "string"
                },
                "name": {
                  "type": "string"
                }
              },
              "additionalProperties": false
            }
          },
          "associatedTaxa": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "name": {
                  "type": "string"
                },
                "relationType": {
                  "type": "string",
                  "enum": [
                    "has food plant",
                    "is food plant of",
                    "has host",
                    "is host of",
                    "has hyperparasite",
                    "is hyperparasite of",
                    "has parasite",
                    "is parasite of",
                    "has substrate",
                    "is substrate of",
                    "has trace",
                    "is trace of",
                    "has background sounds",
                    "is background sound of",
                    "in relation with"
                  ]
                }
              },
              "additionalProperties": false
            }
          },
          "namedAreas": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "areaName": {
                  "type": "string"
                },
                "areaClass": {
                  "type": "string",
                  "enum": [
                    "higherGeography",
                    "continent",
                    "waterBody",
                    "islandGroup",
                    "county",
                    "municipality"
                  ]
                }
              },
              "additionalProperties": false
            }
          }
        },
        "additionalProperties": false
      }
    },
    "identifications": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "taxonRank": {
            "type": "string"
          },
          "scientificName": {
            "type": "object",
            "properties": {
              "fullScientificName": {
                "type": "string"
              },
              "taxonomicStatus": {
                "type": "string"
              },
              "genusOrMonomial": {
                "type": "string"
              },
              "subgenus": {
                "type": "string"
              },
              "specificEpithet": {
                "type": "string"
              },
              "infraspecificEpithet": {
                "type": "string"
              },
              "infraspecificMarker": {
                "type": "string"
              },
              "nameAddendum": {
                "type": "string"
              },
              "authorshipVerbatim": {
                "type": "string"
              },
              "author": {
                "type": "string"
              },
              "year": {
                "type": "string"
              },
              "references": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "titleCitation": {
                      "type": "string"
                    },
                    "citationDetail": {
                      "type": "string"
                    },
                    "uri": {
                      "type": "string",
                      "format": "uri"
                    },
                    "author": {
                      "type": "object",
                      "properties": {
                        "agentText": {
                          "type": "string"
                        },
                        "fullName": {
                          "type": "string"
                        },
                        "organization": {
                          "type": "object",
                          "properties": {
                            "agentText": {
                              "type": "string"
                            },
                            "name": {
                              "type": "string"
                            }
                          },
                          "additionalProperties": false
                        }
                      },
                      "additionalProperties": false
                    },
                    "publicationDate": {
                      "type": "string",
                      "format": "date-time"
                    }
                  },
                  "additionalProperties": false
                }
              },
              "experts": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "agentText": {
                      "type": "string"
                    },
                    "fullName": {
                      "type": "string"
                    },
                    "organization": {
                      "type": "object",
                      "properties": {
                        "agentText": {
                          "type": "string"
                        },
                        "name": {
                          "type": "string"
                        }
                      },
                      "additionalProperties": false
                    }
                  },
                  "additionalProperties": false
                }
              }
            },
            "additionalProperties": false
          },
          "typeStatus": {
            "type": "string",
            "enum": [
              "allotype",
              "epitype",
              "holotype",
              "isoepitype",
              "isolectotype",
              "isoneotype",
              "isosyntype",
              "isotype",
              "lectotype",
              "neotype",
              "paralectotype",
              "paratype",
              "syntype",
              "topotype",
              "type",
              "hapantotype"
            ]
          },
          "dateIdentified": {
            "type": "string",
            "format": "date-time"
          },
          "defaultClassification": {
            "type": "object",
            "properties": {
              "kingdom": {
                "type": "string"
              },
              "phylum": {
                "type": "string"
              },
              "className": {
                "type": "string"
              },
              "order": {
                "type": "string"
              },
              "superFamily": {
                "type": "string"
              },
              "family": {
                "type": "string"
              },
              "genus": {
                "type": "string"
              },
              "subgenus": {
                "type": "string"
              },
              "specificEpithet": {
                "type": "string"
              },
              "infraspecificEpithet": {
                "type": "string"
              },
              "infraspecificRank": {
                "type": "string"
              }
            },
            "additionalProperties": false
          },
          "systemClassification": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "rank": {
                  "type": "string"
                },
                "name": {
                  "type": "string"
                }
              },
              "additionalProperties": false
            }
          },
          "vernacularNames": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "name": {
                  "type": "string"
                },
                "language": {
                  "type": "string"
                },
                "preferred": {
                  "type": "boolean"
                },
                "references": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "titleCitation": {
                        "type": "string"
                      },
                      "citationDetail": {
                        "type": "string"
                      },
                      "uri": {
                        "type": "string",
                        "format": "uri"
                      },
                      "author": {
                        "type": "object",
                        "properties": {
                          "agentText": {
                            "type": "string"
                          },
                          "fullName": {
                            "type": "string"
                          },
                          "organization": {
                            "type": "object",
                            "properties": {
                              "agentText": {
                                "type": "string"
                              },
                              "name": {
                                "type": "string"
                              }
                            },
                            "additionalProperties": false
                          }
                        },
                        "additionalProperties": false
                      },
                      "publicationDate": {
                        "type": "string",
                        "format": "date-time"
                      }
                    },
                    "additionalProperties": false
                  }
                },
                "experts": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "agentText": {
                        "type": "string"
                      },
                      "fullName": {
                        "type": "string"
                      },
                      "organization": {
                        "type": "object",
                        "properties": {
                          "agentText": {
                            "type": "string"
                          },
                          "name": {
                            "type": "string"
                          }
                        },
                        "additionalProperties": false
                      }
                    },
                    "additionalProperties": false
                  }
                }
              },
              "additionalProperties": false
            }
          },
          "identificationQualifiers": {
            "type": "array",
            "items": {
              "type": "string"
            }
          },
          "identifiers": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "agentText": {
                  "type": "string"
                }
              },
              "additionalProperties": false
            }
          }
        },
        "additionalProperties": false
      }
    },
    "previousSourceID": {
      "type": "array",
      "items": {
          "type": "string"
        }
    },
    "previousUnitsText": {
      "type": "string"
    },
    "resourceCreationTechnique": {
      "type": "string"
    },
    "type": {
      "type": "string",
      "enum" : [
        "Collection",
        "StillImage",
        "Sound", 
        "MovingImage",
        "InteractiveResource",
        null
      ]
    },
    "rating": {
      "type": "integer",
      "minimum": -1,
      "maximum": 5
    },
    "modified": {
      "type": "string",
      "format": "date-time"
    },
    "subjectParts": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "subjectOrientations": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "informationWithheld": {
      "type": "string"
    },
    "dataGeneralizations": {
      "type": "string"
    }
  },
  "required": [
   "id",
   "sourceSystem",
    "sourceSystemId",
    "sourceInstitutionID",
    "sourceID",
    "licenseType",
    "license",
    "unitID",
    "collectionType",
    "serviceAccessPoints"
  ],
  "additionalProperties": false
}
