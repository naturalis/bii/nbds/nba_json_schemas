{
  "$schema": "https://json-schema.org/draft/2019-09/schema",
  "$id": "http://api.biodiversitydata.nl/v2/specimen/",
  "version": "2.24",
  "type": "object",
  "properties": {
    "sourceSystem": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string",
          "enum": [
            "CRS",
            "BRAHMS",
            "XC",
            "OBS"
          ]
        },
        "name": {
          "type": "string",
          "enum": [
            "Naturalis - Botany catalogues",
            "Naturalis - Zoology and Geology catalogues",
            "Xeno-canto.org - Bird sounds",
            "Observation.org - Nature observations"
          ]
        }
      },
      "required": [
        "code",
        "name"
      ],
      "additionalProperties": false
    },
    "id": {
      "type": "string"
    },
    "sourceSystemId": {
      "type": "string"
    },
    "recordURI": {
      "type": "string",
      "format": "uri"
    },
    "unitID": {
      "type": "string"
    },
    "unitGUID": {
      "type": "string",
      "format": "uri"
    },
    "collectorsFieldNumber": {
      "type": "string"
    },
    "assemblageID": {
      "type": "string"
    },
    "sourceInstitutionID": {
      "type": "string",
      "enum": [
        "Naturalis Biodiversity Center",
        "Observation International",
        "Stichting Xeno-canto voor Natuurgeluiden"
      ]
    },
    "sourceID": {
      "type": "string",
      "enum": [
        "Brahms",
        "CRS",
        "Xeno-canto",
        "Observation.org"
      ]
    },
    "owner": {
      "type": "string"
    },
    "licenseType": {
      "type": "string",
      "enum": [
        "Copyright"
      ]
    },
    "license": {
      "type": "string",
      "enum": [
	    "CC BY",
	    "CC BY-SA",
	    "CC BY-ND",
	    "CC BY-NC",
	    "CC BY-NC-SA",
	    "CC BY-NC-ND",
	    "CC0",
	    "CC BY 1.0",
	    "CC BY-SA 1.0",
	    "CC BY-ND 1.0",
	    "CC BY-NC 1.0",
	    "CC BY-NC-SA 1.0",
	    "CC BY-NC-ND 1.0",
	    "CC0 1.0",
	    "CC BY 2.0",
	    "CC BY-SA 2.0",
	    "CC BY-ND 2.0",
	    "CC BY-NC 2.0",
	    "CC BY-NC-SA 2.0",
	    "CC BY-NC-ND 2.0",
	    "CC BY 2.5",
	    "CC BY-SA 2.5",
	    "CC BY-ND 2.5",
	    "CC BY-NC 2.5",
	    "CC BY-NC-SA 2.5",
	    "CC BY-NC-ND 2.5",
	    "CC BY 3.0",
	    "CC BY-SA 3.0",
	    "CC BY-ND 3.0",
	    "CC BY-NC 3.0",
	    "CC BY-NC-SA 3.0",
	    "CC BY-NC-ND 3.0",
	    "CC BY 4.0",
	    "CC BY-SA 4.0",
	    "CC BY-ND 4.0",
	    "CC BY-NC 4.0",
	    "CC BY-NC-SA 4.0",
	    "CC BY-NC-ND 4.0",
	    "All rights reserved"
      ]
    },
    "recordBasis": {
      "type": "string"
    },
    "kindOfUnit": {
      "type": "string"
    },
    "collectionType": {
      "type": "string"
    },
    "sex": {
      "type": "string",
      "enum": [
        "female",
        "male",
        "hermaphrodite",
        "mixed",
        "unknowable",
        "undetermined"
      ]
    },
    "phaseOrStage": {
      "type": "string"
    },
    "title": {
      "type": "string"
    },
    "notes": {
      "type": "string"
    },
    "preparationType": {
      "type": "string"
    },
    "numberOfSpecimen": {
      "type": "integer"
    },
    "fromCaptivity": {
      "type": "boolean"
    },
    "objectPublic": {
      "type": "boolean"
    },
    "multiMediaPublic": {
      "type": "boolean"
    },
    "acquiredFrom": {
      "type": "object",
      "properties": {
        "agentText": {
          "type": "string"
        }
      },
      "additionalProperties": false
    },
    "gatheringEvent": {
      "type": "object",
      "properties": {
        "projectTitle": {
          "type": "string"
        },
        "worldRegion": {
          "type": "string"
        },
        "continent": {
          "type": "string"
        },
        "country": {
          "type": "string"
        },
        "iso3166Code": {
          "type": "string"
        },
        "provinceState": {
          "type": "string"
        },
        "island": {
          "type": "string"
        },
        "locality": {
          "type": "string"
        },
        "city": {
          "type": "string"
        },
        "sublocality": {
          "type": "string"
        },
        "localityText": {
          "type": "string"
        },
        "dateTimeBegin": {
          "type": "string",
          "format": "date-time"
        },
        "dateTimeEnd": {
          "type": "string",
          "format": "date-time"
        },
        "altitude": {
          "type": "string"
        },
        "altitudeUnifOfMeasurement": {
          "type": "string"
        },
        "depth": {
          "type": "string"
        },
        "depthUnitOfMeasurement": {
          "type": "string"
        },
        "gatheringPersons": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "agentText": {
                "type": "string"
              },
              "fullName": {
                "type": "string"
              },
              "organization": {
                "type": "object",
                "properties": {
                  "agentText": {
                    "type": "string"
                  },
                  "name": {
                    "type": "string"
                  }
                },
                "additionalProperties": false
              }
            },
            "additionalProperties": false
          }
        },
        "gatheringOrganizations": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "agentText": {
                "type": "string"
              },
              "name": {
                "type": "string"
              }
            },
            "additionalProperties": false
          }
        },
        "siteCoordinates": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "longitudeDecimal": {
                "type": "number",
                "minimum": -180,
                "maximum": 180
              },
              "latitudeDecimal": {
                "type": "number",
                "minimum": -90,
                "maximum": 90
              },
              "gridCellSystem": {
                "type": "string"
              },
              "gridLatitudeDecimal": {
                "type": "number"
              },
              "gridLongitudeDecimal": {
                "type": "number"
              },
              "gridCellCode": {
                "type": "string"
              },
              "gridQualifier": {
                "type": "string"
              },
              "coordinateErrorDistanceInMeters": {
                "type": "number"
              },
              "spatialDatum": {
                "type": "string",
                "enum": [
                  "WGS84",
                  "NAD83",
                  "NAD27"
                ]
              }
            },
            "additionalProperties": false
          }
        },
        "chronoStratigraphy": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "youngRegionalSubstage": {
                "type": "string"
              },
              "youngRegionalStage": {
                "type": "string"
              },
              "youngRegionalSeries": {
                "type": "string"
              },
              "youngDatingQualifier": {
                "type": "string"
              },
              "youngInternSystem": {
                "type": "string"
              },
              "youngInternSubstage": {
                "type": "string"
              },
              "youngInternStage": {
                "type": "string"
              },
              "youngInternSeries": {
                "type": "string"
              },
              "youngInternErathem": {
                "type": "string"
              },
              "youngInternEonothem": {
                "type": "string"
              },
              "youngChronoName": {
                "type": "string"
              },
              "youngCertainty": {
                "type": "string"
              },
              "oldDatingQualifier": {
                "type": "string"
              },
              "chronoPreferredFlag": {
                "type": "boolean"
              },
              "oldRegionalSubstage": {
                "type": "string"
              },
              "oldRegionalStage": {
                "type": "string"
              },
              "oldRegionalSeries": {
                "type": "string"
              },
              "oldInternSystem": {
                "type": "string"
              },
              "oldInternSubstage": {
                "type": "string"
              },
              "oldInternStage": {
                "type": "string"
              },
              "oldInternSeries": {
                "type": "string"
              },
              "oldInternErathem": {
                "type": "string"
              },
              "oldInternEonothem": {
                "type": "string"
              },
              "oldChronoName": {
                "type": "string"
              },
              "chronoIdentifier": {
                "type": "string"
              },
              "oldCertainty": {
                "type": "string"
              }
            },
            "additionalProperties": false
          }
        },
        "lithoStratigraphy": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "qualifier": {
                "type": "string"
              },
              "preferredFlag": {
                "type": "boolean"
              },
              "member2": {
                "type": "string"
              },
              "member": {
                "type": "string"
              },
              "informalName2": {
                "type": "string"
              },
              "informalName": {
                "type": "string"
              },
              "importedName2": {
                "type": "string"
              },
              "importedName1": {
                "type": "string"
              },
              "lithoIdentifier": {
                "type": "string"
              },
              "formation2": {
                "type": "string"
              },
              "formationGroup2": {
                "type": "string"
              },
              "formationGroup": {
                "type": "string"
              },
              "formation": {
                "type": "string"
              },
              "certainty2": {
                "type": "string"
              },
              "certainty": {
                "type": "string"
              },
              "bed2": {
                "type": "string"
              },
              "bed": {
                "type": "string"
              }
            },
            "additionalProperties": false
          }
        },
        "bioStratigraphy": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "youngBioDatingQualifier": {
                "type": "string"
              },
              "youngBioName": {
                "type": "string"
              },
              "youngFossilZone": {
                "type": "string"
              },
              "youngFossilSubZone": {
                "type": "string"
              },
              "youngBioCertainty": {
                "type": "string"
              },
              "youngStratType": {
                "type": "string"
              },
              "bioDatingQualifier": {
                "type": "string"
              },
              "bioPreferredFlag": {
                "type": "boolean"
              },
              "rangePosition": {
                "type": "string"
              },
              "oldBioName": {
                "type": "string"
              },
              "bioIdentifier": {
                "type": "string"
              },
              "oldFossilzone": {
                "type": "string"
              },
              "oldFossilSubzone": {
                "type": "string"
              },
              "oldBioCertainty": {
                "type": "string"
              },
              "oldBioStratType": {
                "type": "string"
              }
            },
            "additionalProperties": false
          }
        },
        "associatedTaxa": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "name": {
                "type": "string"
              },
              "relationType": {
                "type": "string",
                "enum": [
                  "has food plant",
                  "is food plant of",
                  "has host",
                  "is host of",
                  "has hyperparasite",
                  "is hyperparasite of",
                  "has parasite",
                  "is parasite of",
                  "has substrate",
                  "is substrate of",
                  "has trace",
                  "is trace of",
                  "has background sounds",
                  "is background sound of",
                  "in relation with"
                ]
              }
            },
            "additionalProperties": false
          }
        },
        "namedAreas": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "areaName": {
                "type": "string"
              },
              "areaClass": {
                "type": "string",
                "enum": [
                  "higherGeography",
                  "continent",
                  "waterBody",
                  "islandGroup",
                  "county",
                  "municipality"
                ]
              }
            },
            "additionalProperties": false
          }
        },
        "biotopeText": {
          "type": "string"
        },
        "dateText": {
          "type": "string"
        },
        "behavior": {
          "type": "string"
        },
        "method": {
          "type": "string"
        },
        "code": {
          "type": "string"
        },
        "establishmentMeans": {
          "type": "string"
        }
      },
      "additionalProperties": false
    },
    "identifications": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "taxonRank": {
            "type": "string"
          },
          "scientificName": {
            "type": "object",
            "properties": {
              "fullScientificName": {
                "type": "string"
              },
              "taxonomicStatus": {
                "type": "string"
              },
              "genusOrMonomial": {
                "type": "string"
              },
              "subgenus": {
                "type": "string"
              },
              "specificEpithet": {
                "type": "string"
              },
              "infraspecificEpithet": {
                "type": "string"
              },
              "infraspecificMarker": {
                "type": "string"
              },
              "nameAddendum": {
                "type": "string"
              },
              "authorshipVerbatim": {
                "type": "string"
              },
              "author": {
                "type": "string"
              },
              "year": {
                "type": "string"
              },
              "references": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "titleCitation": {
                      "type": "string"
                    },
                    "citationDetail": {
                      "type": "string"
                    },
                    "uri": {
                      "type": "string"
                    },
                    "author": {
                      "type": "object",
                      "properties": {
                        "agentText": {
                          "type": "string"
                        },
                        "fullName": {
                          "type": "string"
                        },
                        "organization": {
                          "type": "object",
                          "properties": {
                            "agentText": {
                              "type": "string"
                            },
                            "name": {
                              "type": "string"
                            }
                          },
                          "additionalProperties": false
                        }
                      },
                      "additionalProperties": false
                    },
                    "publicationDate": {
                      "type": "string",
                      "format": "date-time"
                    }
                  },
                  "additionalProperties": false
                }
              },
              "experts": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "agentText": {
                      "type": "string"
                    },
                    "fullName": {
                      "type": "string"
                    },
                    "organization": {
                      "type": "object",
                      "properties": {
                        "agentText": {
                          "type": "string"
                        },
                        "name": {
                          "type": "string"
                        }
                      },
                      "additionalProperties": false
                    }
                  },
                  "additionalProperties": false
                }
              }
            },
            "additionalProperties": false
          },
          "typeStatus": {
            "type": "string",
            "enum": [
              "allotype",
              "epitype",
              "holotype",
              "isoepitype",
              "isolectotype",
              "isoneotype",
              "isosyntype",
              "isotype",
              "lectotype",
              "neotype",
              "paralectotype",
              "paratype",
              "syntype",
              "topotype",
              "type",
              "hapantotype"
            ]
          },
          "dateIdentified": {
            "type": "string",
            "format": "date-time"
          },
          "defaultClassification": {
            "type": "object",
            "properties": {
              "domain": {
                "type": "string"
              },
              "kingdom": {
                "type": "string"
              },
              "subKingdom": {
                "type": "string"
              },
              "phylum": {
                "type": "string"
              },
              "subPhylum": {
                "type": "string"
              },
              "superClass": {
                "type": "string"
              },
              "className": {
                "type": "string"
              },
              "subClass": {
                "type": "string"
              },
              "superOrder": {
                "type": "string"
              },
              "order": {
                "type": "string"
              },
              "subOrder": {
                "type": "string"
              },
              "infraOrder": {
                "type": "string"
              },
              "superFamily": {
                "type": "string"
              },
              "family": {
                "type": "string"
              },
              "subFamily": {
                "type": "string"
              },
              "tribe": {
                "type": "string"
              },
              "subTribe": {
                "type": "string"
              },
              "genus": {
                "type": "string"
              },
              "subgenus": {
                "type": "string"
              },
              "specificEpithet": {
                "type": "string"
              },
              "infraspecificEpithet": {
                "type": "string"
              },
              "infraspecificRank": {
                "type": "string"
              }
            },
            "additionalProperties": false
          },
          "systemClassification": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "rank": {
                  "type": "string"
                },
                "name": {
                  "type": "string"
                }
              },
              "additionalProperties": false
            }
          },
          "vernacularNames": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "name": {
                  "type": "string"
                },
                "language": {
                  "type": "string"
                },
                "preferred": {
                  "type": "boolean"
                },
                "references": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "titleCitation": {
                        "type": "string"
                      },
                      "citationDetail": {
                        "type": "string"
                      },
                      "uri": {
                        "type": "string"
                      },
                      "author": {
                        "type": "object",
                        "properties": {
                          "agentText": {
                            "type": "string"
                          },
                          "fullName": {
                            "type": "string"
                          },
                          "organization": {
                            "type": "object",
                            "properties": {
                              "agentText": {
                                "type": "string"
                              },
                              "name": {
                                "type": "string"
                              }
                            },
                            "additionalProperties": false
                          }
                        },
                        "additionalProperties": false
                      },
                      "publicationDate": {
                        "type": "string",
                        "format": "date-time"
                      }
                    },
                    "additionalProperties": false
                  }
                },
                "experts": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "agentText": {
                        "type": "string"
                      },
                      "fullName": {
                        "type": "string"
                      },
                      "organization": {
                        "type": "object",
                        "properties": {
                          "agentText": {
                            "type": "string"
                          },
                          "name": {
                            "type": "string"
                          }
                        },
                        "additionalProperties": false
                      }
                    },
                    "additionalProperties": false
                  }
                }
              },
              "additionalProperties": false
            }
          },
          "identificationQualifiers": {
            "type": "array",
            "items": {
              "type": "string"
            }
          },
          "identifiers": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "agentText": {
                  "type": "string"
                }
              },
              "additionalProperties": false
            }
          },
          "preferred": {
            "type": "boolean"
          },
          "verificationStatus": {
            "type": "string"
          },
          "rockType": {
            "type": "string"
          },
          "associatedFossilAssemblage": {
            "type": "string"
          },
          "rockMineralUsage": {
            "type": "string"
          },
          "associatedMineralName": {
            "type": "string"
          },
          "remarks": {
            "type": "string"
          }
        },
        "additionalProperties": false
      }
    },
    "associatedMultiMediaUris": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "accessUri": {
            "type": "string",
            "format": "uri"
          },
          "format": {
            "type": "string",
            "enum": [
              "image/jpeg",
              "audio/mp3",
              "image/png",
              "video/mp4",
              "application/pdf"
            ]
          },
          "variant": {
            "type": "string"
          }
        },
        "additionalProperties": false
      }
    },
    "previousSourceID": {
      "type": "array",
      "items": {
          "type": "string"
       }
    },
    "previousUnitsText": {
      "type": "string"
    },
    "informationWithheld": {
      "type": "string"
    },
    "dataGeneralizations": {
      "type": "string"
    },
    "modified": {
      "type": "string",
      "format": "date-time"
    }
  },
  "required": [
    "id",
    "sourceSystem",
    "sourceSystemId",
    "unitID",
    "sourceInstitutionID",
    "sourceID",
    "licenseType",
    "license",
    "recordBasis",
    "collectionType",
    "objectPublic",
    "identifications"
  ],
  "additionalProperties": false
}