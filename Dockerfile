FROM alpine:3
LABEL maintainer="Maarten Schermer <maarten.schermer@naturalis.nl>"

WORKDIR /
ARG branch_name=""
ARG repo_url=""
ENV BRANCH="${branch_name}"
ENV REPO="${repo_url}"

RUN mkdir -p /schemas
RUN mkdir -p /repo

RUN apk add --no-cache git=~2
COPY clone.sh .

CMD ["/clone.sh"]
#CMD ["tail", "-f", "/dev/null"]