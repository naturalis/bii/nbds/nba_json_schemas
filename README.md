# NBA JSON-schemas

This repo contains JSON-schema files for validation of documents intended for import into the Naturalis Document Store (NDS), the datastore underlying the Naturalis Biodiversity API (NBA).

The NDS contains three indexes, specimen (for specimen and occurrence data), taxon (containing taxonomic data) and multimedia (containing multimedia-metadata associated with records from either the specimen or the taxon index). Each index collects normalized, ABCD-compliant documents from various sources, thus presenting centralized access to a wide range of biodiversity data.

## JSON schema specification
See: <http://json-schema.org/specification-links.html>

### date-time formats
In its literal form, the JSON schema string format "date-time" validates against the [RFC3339](https://www.ietf.org/rfc/rfc3339.txt) specification. However, the NDS accepts dates in ISO8601, which is less strict. For this purpose, the [PHP validator](https://github.com/naturalis/nba_json_schemas/blob/master/tools/validator/PHP/) in the tools folder explicitly replaces the default date-time validation procedure with a ISO8601-compliant one.

### duplicate elements
The JSON specification (<https://tools.ietf.org/html/rfc7159>) forbids duplicate elements:
> An object structure is represented as a pair of curly brackets surrounding zero or more name/value pairs (or members). A name is a string. A single colon comes after each name, separating the name from the value. A single comma separates a value from a following name. The names within an object **SHOULD** be unique.
(emphasis added)

However, the RFC goes on to suggest that this is to guarantee interoperability across software implementations, rather than a semantic feature. Hence, this validator does not check for, and will not halt or report on duplicate elements, should they be present. It will succesfully validate a document despite the presence of duplicates, sending it onwards through the data pipeline. However, they will be rejected in the final step (data loading into the document store), making them a potential cause of "missing documents" when comparing the numbers of validated and loaded documents. We currently have no solution for this issue, so we urge data providers to make sure there are no duplicate elements in their documents. Most standard JSON-encoders silently ignore duplicate elements, so in most cases the problem is unlikely to occur.

## Building a container
```console
docker build -t naturalis/nba_json_schemas:latest .
```

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

* `pre-commit autoupdate`
* `pre-commit install`
