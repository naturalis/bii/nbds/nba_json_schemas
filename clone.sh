#!/bin/sh
#
# clone.sh : clone gitlab repo containing the nba json schemas and move
# them to the required directory: /schemas
#
# NOTE: the branch name of the git repo, serves as the tag name of the
# docker image.

# pull schema repo
cd /repo || (echo "error: failed to cd into /repo" && exit 1)
rm -R ./* 2> /dev/null
if [ -z "${BRANCH}" ];
then
  echo "info: checking out nba json schemas main branch"
  if ! git clone "${REPO}" > /dev/null 2>&1
  then
    echo "error: failed to clone the main branch of the repository ${REPO}"
    exit 1
  fi
else
  echo "info: checking out nba json schemas branch: ${BRANCH}"
  if ! git clone "${REPO}" --branch "${BRANCH}" > /dev/null 2>&1
  then
    echo "error: failed to clone the ${BRANCH} branch of the repository ${REPO}"
    exit 1
  fi
fi

# move json schemas to /schemas directory
cd nba_json_schemas || (echo "error: failed to cd to /nba_json_schemas" && exit 1)
rm -R /schemas/* 2> /dev/null
find . -mindepth 1 -maxdepth 1 -type d -not -name ".git" -exec mv {} /schemas/. \;
cd /repo && rm -R nba_json_schemas/

echo "info: updated nba json schemas"
